@createLists @all
Feature: User should be able to create lists on board

  Background:
    Given user navigates to url "baseURL"
    And user logs in as
      | email      | password      |
      | user.email | user.password |
    And user creates new board and puts its name to variable "boardName"


  Scenario Outline: user can create 1+ lists on dashboard
    When Board > user creates new list with name "<name1>"
    Then the following lists are displayed
      | <name1> |

    And Board > user creates new list with name "<name2>"
    Then the following lists are displayed
      | <name1> |
      | <name2> |

    And Board > user creates new list with name "<name3>"
    Then the following lists are displayed
      | <name1> |
      | <name2> |
      | <name3> |

    And Board > user creates new list with name "<name4>"
    Then the following lists are displayed
      | <name1> |
      | <name2> |
      | <name3> |
      | <name4> |

    And Board > user creates new list with name "<name5>"
    Then the following lists are displayed
      | <name1> |
      | <name2> |
      | <name3> |
      | <name4> |
      | <name5> |

    Examples:
      | name1                       | name2          | name3          | name4           | name5                   |
      | a                           | $              | 1              | 1 % _           | ONLY CAPS               |
      | <script>alert('1')</script> | 512SymbolsName | 512SymbolsName | !@#$%^&*()<>?/\ | Список для тестирования |


  Scenario: User should not be able to create list with empty name
    When Board > user opens create list menu and types list name ""
    And  Board > user clicks on "Create list button"
    Then Board > check there are no lists displayed

    When Board > user opens create list menu and types list name "            "
    And  Board > user clicks on "Create list button"
    Then Board > check there are no lists displayed

    When user press "ENTER"
    Then Board > check there are no lists displayed


  Scenario: User should not be able to create list with name with more than 512 symbols length
    When Board > user creates new list with name "513SymbolsName"
    Then the following lists are displayed
      | 512SymbolsName |


  Scenario: user can create new list via pressing ENTER
    When Board > user opens create list menu and types list name "Created via ENTER"
    And user press "ENTER"
    Then the following lists are displayed
      | Created via ENTER |