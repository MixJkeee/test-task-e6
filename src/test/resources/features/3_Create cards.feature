@createCards @all
Feature: User should be able to create cards in list

  Background:
    Given user navigates to url "baseURL"
    And user logs in as
      | email      | password      |
      | user.email | user.password |
    And user creates new board and puts its name to variable "boardName"
    And Board > user creates new list with name "Cards creation test"


  Scenario Outline: user can add 1+ cards to a list
    When Board > user adds new card with title "<name1>" to list "Cards creation test"
    Then the following cards are displayed in "Cards creation test" list
      | <name1> |

    When Board > user adds new card with title "<name2>" to list "Cards creation test"
    Then the following cards are displayed in "Cards creation test" list
      | <name1> |
      | <name2> |

    When Board > user adds new card with title "<name3>" to list "Cards creation test"
    Then the following cards are displayed in "Cards creation test" list
      | <name1> |
      | <name2> |
      | <name3> |

    When Board > user adds new card with title "<name4>" to list "Cards creation test"
    Then the following cards are displayed in "Cards creation test" list
      | <name1> |
      | <name2> |
      | <name3> |
      | <name4> |

    When Board > user adds new card with title "<name5>" to list "Cards creation test"
    Then the following cards are displayed in "Cards creation test" list
      | <name1> |
      | <name2> |
      | <name3> |
      | <name4> |
      | <name5> |

    Examples:
      | name1                       | name2          | name3          | name4           | name5                   |
      | a                           | $              | 1              | 1               | ONLY CAPS               |
      | <script>alert('1')</script> | 512SymbolsName | 512SymbolsName | !@#$%^&*()<>?/\ | Карточка для тестирования |


  Scenario: user should be able to create card with 190530 symbols length
    Given user generates random string with 190530 symbols length and saves to variable "190530SymbolsName"
    And user copies value "190530SymbolsName" to clipboard

    When Board > user opens card title input in "Cards creation test" list
    And user paste value from clipboard
    And user press "ENTER"

    Then Index page > check element "Notification" is hidden
    And the following cards are displayed in "Cards creation test" list
      | 190530SymbolsName |


  Scenario: user should not be able to create card with 190531 symbols length
    Given user generates random string with 190531 symbols length and saves to variable "190531SymbolsName"
    And user copies value "190531SymbolsName" to clipboard

    When Board > user opens card title input in "Cards creation test" list
    And user paste value from clipboard
    And user press "ENTER"

    Then Index page > check element "Notification" is displayed
    And Board > there are no cards displayed in "Cards creation test" list


  Scenario: user should not be able to create card with empty title

    When Board > user types "" into new card name input in "Cards creation test" list
    And user press "ENTER"

    Then Index page > check element "Notification" is hidden
    And Board > there are no cards displayed in "Cards creation test" list

    When Board > user types "         " into new card name input in "Cards creation test" list
    And user press "ENTER"

    Then Index page > check element "Notification" is hidden
    And Board > there are no cards displayed in "Cards creation test" list
