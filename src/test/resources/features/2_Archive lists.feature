@all @archiveLists
Feature: User should be able to archive lists on board

  Background:
    Given user navigates to url "baseURL"
    And user logs in as
      | email      | password      |
      | user.email | user.password |
    And user creates new board and puts its name to variable "boardName"


  Scenario: archiving lists on different positions
    Given Board > user creates the following lists
      | list 1 |
      | list 2 |
      | list 3 |
      | list 4 |
      | list 5 |
      | list 6 |

    When Board > user archives list "list 1"
    Then the following lists are displayed
      | list 2 |
      | list 3 |
      | list 4 |
      | list 5 |
      | list 6 |

    When Board > user archives list "list 3"
    Then the following lists are displayed
      | list 2 |
      | list 4 |
      | list 5 |
      | list 6 |

    When Board > user archives list "list 6"
    Then the following lists are displayed
      | list 2 |
      | list 4 |
      | list 5 |


  Scenario: user can archive the last list on board
    Given Board > user creates the following lists
      | Last list        |
      | Before last list |

    When Board > user archives list "Before last list"
    And  Board > user archives list "Last list"
    Then Board > check there are no lists displayed


  Scenario: user should be able to archive list with cards
    When Board > user creates new list with name "Archive with cards"
    And  Board > user adds new card with title "Card 1" to list "Archive with cards"
    And  Board > user adds new card with title "Card 2" to list "Archive with cards"
    And  Board > user archives list "Archive with cards"
    Then Board > check there are no lists displayed
