package com.eagle6.test.project.pages;

import lombok.Getter;
import org.openqa.selenium.WebDriver;

import static ru.yandex.qatools.htmlelements.loader.HtmlElementLoader.createPageObject;

@Getter
public class BasePage {

    protected WebDriver webDriver;

    public BasePage(WebDriver driver) {
        webDriver = driver;
    }

    public <T extends BasePage> T getPage(Class<T> pageType) {
        return createPageObject(pageType, webDriver);
    }
}
