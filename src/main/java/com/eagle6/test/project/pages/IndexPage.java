package com.eagle6.test.project.pages;

import com.eagle6.test.project.components.common.CreateBoardPopover;
import com.eagle6.test.project.components.common.CreateMenuPopover;
import com.eagle6.test.project.components.common.Header;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.TextBlock;

import static com.eagle6.test.project.util.WebElementsUtils.click;
import static com.eagle6.test.project.util.WebElementsUtils.waitFor;
import static ru.yandex.qatools.htmlelements.matchers.WebElementMatchers.isDisplayed;

@Getter
@Log4j2
public class IndexPage extends BasePage {

    @Name("Notification")
    @FindBy(css = "#notification")
    protected TextBlock notification;

    protected Header header;

    protected CreateMenuPopover createMenu;
    protected CreateBoardPopover createBoardPopover;

    public IndexPage(WebDriver driver) {
        super(driver);
    }

    public BoardPage createNewBoard(String boardName) {
        click(header.getCreateButton());
        click(createMenu.getCreateBoardButton());
        createBoardPopover.createBoardWithName(boardName);
        BoardPage boardPage = getPage(BoardPage.class);
        waitFor(boardPage.getBoardHeader(), isDisplayed());
        return boardPage;
    }

    public AllBoardsPage openAllBoardsPage() {
        click(header.getHome());
        return getPage(AllBoardsPage.class);
    }
}
