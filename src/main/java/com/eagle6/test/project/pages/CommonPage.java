package com.eagle6.test.project.pages;

import com.eagle6.test.project.components.NavBar;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.htmlelements.annotations.Name;

import static com.eagle6.test.project.util.WebElementsUtils.click;


@Getter
public class CommonPage extends BasePage {

    @Name("Common page navbar")
    private NavBar navBar;

    public CommonPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage openLoginPage() {
        click(navBar.getLogin());
        return getPage(LoginPage.class);
    }
}
