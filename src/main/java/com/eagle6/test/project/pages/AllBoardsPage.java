package com.eagle6.test.project.pages;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebDriver;

@Getter
@Log4j2
public class AllBoardsPage extends IndexPage {
    public AllBoardsPage(WebDriver driver) {
        super(driver);
    }
}
