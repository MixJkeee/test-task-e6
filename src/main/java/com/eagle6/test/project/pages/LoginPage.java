package com.eagle6.test.project.pages;

import com.eagle6.test.project.components.login.AtlassianLoginForm;
import com.eagle6.test.project.components.login.TrelloLoginForm;
import com.eagle6.test.project.model.User;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebDriver;

import static com.eagle6.test.project.util.WebElementsUtils.*;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.Matchers.not;
import static ru.yandex.qatools.htmlelements.matchers.WebElementMatchers.hasValue;
import static ru.yandex.qatools.htmlelements.matchers.WebElementMatchers.isDisplayed;

@Getter
@Log4j2
public class LoginPage extends BasePage {

    private TrelloLoginForm trelloLoginForm;
    private AtlassianLoginForm atlassianLoginForm;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public AllBoardsPage loginViaAtlassianAccountAs(User user) {
        log.info("Logging in as {}", user);
        trelloLoginForm.openAtlassianLoginPageAs(user.getEmail());
        atlassianLoginForm.loginAs(user);
        AllBoardsPage allBoardsPage = getPage(AllBoardsPage.class);
        waitFor(allBoardsPage.getHeader(), isDisplayed(), 30000);
        return allBoardsPage;
    }
}
