package com.eagle6.test.project.pages;

import com.eagle6.test.project.components.board.BoardContent;
import com.eagle6.test.project.components.board.BoardHeader;
import com.eagle6.test.project.components.board.ListMenu;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebDriver;

import static com.eagle6.test.project.util.WebElementsUtils.waitFor;
import static ru.yandex.qatools.htmlelements.matchers.WebElementMatchers.isDisplayed;

@Getter
@Log4j2
public class BoardPage extends IndexPage {
    private BoardHeader boardHeader;
    private BoardContent boardContent;
    private ListMenu listMenu;

    public BoardPage createNewList(String listName) {
        boardContent.createNewList(listName);
        return this;
    }

    public BoardPage archiveList(String listName) {
        boardContent.openCardListMenu(listName);
        waitFor(listMenu, isDisplayed()).click(listMenu.getArchiveListButton());
        return this;
    }

    public BoardPage openAddListMenu() {
        boardContent.openCreateListMenu();
        return this;
    }

    public BoardPage(WebDriver driver) {
        super(driver);
    }
}
