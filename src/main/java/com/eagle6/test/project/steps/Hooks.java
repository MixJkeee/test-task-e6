package com.eagle6.test.project.steps;

import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import lombok.SneakyThrows;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.eagle6.test.project.util.WebDriverFactory.getWebDriver;
import static io.qameta.allure.Allure.addAttachment;
import static java.util.Optional.ofNullable;

public class Hooks {

    public static ThreadLocal<WebDriver> webDriverThreadLocal = new ThreadLocal<>();

    @Before
    public void setUp() {
        webDriverThreadLocal.set(getWebDriver());
    }

    @After
    @SneakyThrows(IOException.class)
    public void afterFail(Scenario scenario) {
        if (scenario.isFailed()) {
            byte[] screenshot = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
            try (InputStream is = new ByteArrayInputStream(screenshot)) {
                addAttachment(scenario.getName(), is);
            }
        }
    }

    @After(order = 2000)
    public void tearDown() {
        ofNullable(webDriverThreadLocal.get()).ifPresent(WebDriver::quit);
    }

    public static WebDriver getDriver() {
        return ofNullable(webDriverThreadLocal.get()).orElseThrow(RuntimeException::new);
    }
}
