package com.eagle6.test.project.steps;

import com.eagle6.test.project.components.board.BoardContent;
import com.eagle6.test.project.components.board.CardComposer;
import com.eagle6.test.project.components.board.CardList;
import com.eagle6.test.project.model.StringPropOrValue;
import com.eagle6.test.project.pages.BoardPage;
import com.eagle6.test.project.pages.IndexPage;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.matchers.decorators.Waiter;

import java.util.List;

import static com.eagle6.test.project.util.ReflectionUtils.getWebElementByName;
import static com.eagle6.test.project.util.WebElementsUtils.click;
import static io.qameta.allure.Allure.addAttachment;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static ru.yandex.qatools.htmlelements.matchers.MatcherDecorators.should;
import static ru.yandex.qatools.htmlelements.matchers.MatcherDecorators.timeoutHasExpired;
import static ru.yandex.qatools.htmlelements.matchers.WebElementMatchers.isDisplayed;

@Log4j2
public class TrelloSteps extends BaseStep {

    private static final Waiter FIVE_SECONDS_EXPIRED = timeoutHasExpired(5000);

    //putting boardName to variable in order to remove this board in future
    @When("^user creates new board and puts its name to variable \"([^\"]*)\"$")
    public void createNewBoard(String varName) {
        String boardName = RandomStringUtils.random(10, true, true);
        addAttachment("Created board name", "Board name is:" + boardName);
        withPage(IndexPage.class, indexPage -> indexPage.createNewBoard(boardName));
        scenarioVariables.put(varName, boardName);
    }

    @When("Board > user creates new list with name \"{propertyOrValue}\"")
    public void createNewList(StringPropOrValue listName) {
        addAttachment("List name", "Created list name: " + listName.getValue());
        withPage(BoardPage.class, boardPage -> boardPage.createNewList(listName.getValue()));
    }

    @When("^Board > user creates the following lists$")
    public void createCoupleOfLists(List<String> listNames) {
        listNames.stream().map(this::getPropertyOrStringVariableOrValue)
                .forEach(listName -> withPage(BoardPage.class, page -> page.createNewList(listName)));
    }

    @When("^Board > user archives list \"([^\"]*)\"$")
    public void archiveList(String listNameOrPropertyOrVar) {
        String listName = getPropertyOrStringVariableOrValue(listNameOrPropertyOrVar);
        addAttachment("List to archive", "List to be archived " + listName);
        withPage(BoardPage.class, boardPage -> boardPage.archiveList(listName));
    }

    @When("^Board > user opens create list menu and types list name \"([^\"]*)\"$")
    public void openCreateListMenu(String listNameOrPropertyOrVar) {
        String listName = getPropertyOrStringVariableOrValue(listNameOrPropertyOrVar);
        addAttachment("List name", "List name " + listName);
        withPage(BoardPage.class, boardPage -> boardPage.openAddListMenu()
                .getBoardContent().setListName(listName));
    }

    @Then("^Board > user clicks on \"([^\"]*)\"$")
    public void clickOnElementOnBoard(String elementName) {
        withElement(BoardContent.class, boardContent -> click(getWebElementByName(boardContent, elementName)));
    }

    @When("^Board > user types \"([^\"]*)\" into new card name input in \"([^\"]*)\" list$")
    public void typeNewCardName(String cardNameOrPropOrVar, String listNameOrPropOrVar) {
        String listName = getPropertyOrStringVariableOrValue(listNameOrPropOrVar);
        String cardName = getPropertyOrStringVariableOrValue(cardNameOrPropOrVar);
        addAttachment("Typing card name", String.format("Typing \"%s\" into card name input of \"%s\" list", cardName, listName));
        withElement(BoardContent.class, boardContent ->
                boardContent.findCardList(listName).openCardComposer().typeCardName(cardName));
    }

    @When("^Board > user adds new card with title \"([^\"]*)\" to list \"([^\"]*)\"$")
    public void addCardToList(String cardNameOfPropOrVar, String listNameOrPropOrVar) {
        String cardName = getPropertyOrStringVariableOrValue(cardNameOfPropOrVar);
        String listName = getPropertyOrStringVariableOrValue(listNameOrPropOrVar);
        addAttachment("New card", String.format("Adding card with name \"%s\" to list \"%s\"", cardName, listName));
        withElement(BoardContent.class, boardContent -> boardContent.createCardInList(listName, cardName));
    }

    @When("^Board > user opens card title input in \"([^\"]*)\" list$")
    public void openCardComposerInList(String listNameOrPropOrVar) {
        String listName = getPropertyOrStringVariableOrValue(listNameOrPropOrVar);
        addAttachment("Card menu list", "Open add card menu in list: " + listName);
        withElement(BoardContent.class, boardContent -> {
            CardComposer cardComposer = boardContent.findCardList(listName)
                    .openCardComposer();
            cardComposer.click(cardComposer.getCardTitleInput());
        });
    }

    @Then("Board > check there are no lists displayed")
    public void checkListsAreNotDisplayedAtAll() {
        withElement(BoardContent.class, boardContent -> assertThat(boardContent.getCardLists(), empty()));
    }

    @Then("Board > there are no cards displayed in \"([^\"]*)\" list")
    public void checkThereAreNoCardsDisplayed(String listNameOrPropOrVar) {
        String listName = getPropertyOrStringVariableOrValue(listNameOrPropOrVar);
        addAttachment("Check list is empty", "List should be empty: " + listName);
        withElement(BoardContent.class, boardContent ->
                assertThat(boardContent.findCardList(listName).getCards(), empty()));
    }

    @Then("^the following lists are displayed$")
    public void checkDisplayedLists(List<String> listNamesOrProperties) {
        List<String> expectedListNames = listNamesOrProperties.stream()
                .map(this::getPropertyOrStringVariableOrValue)
                .collect(toList());
        addAttachment("Expected lists", "Expected lists:\n" + expectedListNames);
        withElement(BoardContent.class, boardContent -> {
            assertThat(boardContent.getCardLists(), should(hasSize(expectedListNames.size())).whileWaitingUntil(FIVE_SECONDS_EXPIRED));
            assertThat(boardContent.getCardsListsNames(), equalTo(expectedListNames));
        });
    }

    @Then("^the following cards are displayed in \"([^\"]*)\" list$")
    public void checkCardsInList(String listNameOrPropOrVar, List<String> cardsNamesOrProperties) {
        String listName = getPropertyOrStringVariableOrValue(listNameOrPropOrVar);
        List<String> cardsNames = cardsNamesOrProperties.stream()
                .map(this::getPropertyOrStringVariableOrValue)
                .collect(toList());
        addAttachment("Expected cards", "Expected cards in \"" + listName + "\" are:\n" + cardsNames);
        withElement(BoardContent.class, boardContent -> {
            CardList cardList = boardContent.findCardList(listName);
            assertThat(cardList.getCards(), should(hasSize(cardsNames.size())).whileWaitingUntil(FIVE_SECONDS_EXPIRED));
            assertThat(cardList.getDisplayedCardsNames(), equalTo(cardsNames));
        });
    }

    @Then("Index page > check element \"([^\"]*)\" is (displayed|hidden)")
    public void checkElementVisibility(String elementName, String conditionDesc) {
        boolean shouldBeDisplayed = conditionDesc.equals("displayed");
        withPage(IndexPage.class, indexPage -> {
            WebElement element = getWebElementByName(indexPage, elementName);
            if (shouldBeDisplayed) {
                assertThat(element, should(isDisplayed()).whileWaitingUntil(FIVE_SECONDS_EXPIRED));
            } else {
                assertThat(element, not(isDisplayed()));
            }
        });
    }


}
