package com.eagle6.test.project.steps;

import com.eagle6.test.project.model.StringPropOrValue;
import com.eagle6.test.project.model.User;
import com.eagle6.test.project.pages.CommonPage;
import io.cucumber.java.en.When;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.Keys;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.time.Duration;

import static io.qameta.allure.Allure.addAttachment;
import static java.awt.Toolkit.getDefaultToolkit;
import static org.openqa.selenium.Keys.LEFT_CONTROL;

@Log4j2
public class CommonSteps extends BaseStep {

    private Clipboard getClipboard() {
        return getDefaultToolkit().getSystemClipboard();
    }

    @When("^user generates random string with (\\d+) symbols length and saves to variable \"([^\"]*)\"$")
    public void generateRandomString(int length, String varName) {
        String generatedString = RandomStringUtils.random(length, true, true);
        addAttachment("Generated string", "Generated string is:\n" + generatedString);
        scenarioVariables.put(varName, generatedString);
    }

    @When("user navigates to url \"{propertyOrValue}\"")
    public void navigateTo(StringPropOrValue url) {
        addAttachment("URL", "URL is " + url.getValue());
        driver.get(url.getValue());
    }

    @When("^user logs in as$")
    public void loginAs(User user) {
        addAttachment("Current user", "Current user is " + user);
        withPage(CommonPage.class, commonPage -> commonPage.openLoginPage().loginViaAtlassianAccountAs(user));
    }

    @When("user press \"{key}\"")
    public void pressKey(Keys key) {
        addAttachment("Key to press", "Pressing key " + key);
        actions().sendKeys(key).build().perform();
    }

    @When("user copies value \"([^\"]*)\" to clipboard")
    public void copyToClipboard(String propOrVarOrValue) {
        String stringToCopy = getPropertyOrStringVariableOrValue(propOrVarOrValue);
        addAttachment("Copy value", "Value to copy:\n" + stringToCopy);
        StringSelection selection = new StringSelection(stringToCopy);
        getClipboard().setContents(selection, null);
    }

    @When("user paste value from clipboard")
    public void pasteFromClipboard() {
        actions().keyDown(LEFT_CONTROL).sendKeys("v").keyUp(LEFT_CONTROL)
                .pause(Duration.ofSeconds(2)).build().perform();
    }
}
