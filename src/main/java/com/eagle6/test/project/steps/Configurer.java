package com.eagle6.test.project.steps;

import com.eagle6.test.project.model.StringPropOrValue;
import com.eagle6.test.project.model.User;
import io.cucumber.core.api.TypeRegistry;
import io.cucumber.core.api.TypeRegistryConfigurer;
import io.cucumber.cucumberexpressions.ParameterType;
import io.cucumber.cucumberexpressions.Transformer;
import io.cucumber.datatable.DataTableType;
import org.openqa.selenium.Keys;

import java.util.Locale;

import static java.util.Locale.ENGLISH;

public class Configurer implements TypeRegistryConfigurer {
    @Override
    public Locale locale() {
        return ENGLISH;
    }

    @Override
    public void configureTypeRegistry(TypeRegistry typeRegistry) {
        typeRegistry.defineDataTableType(new DataTableType(User.class, User::createUser));
        typeRegistry.defineParameterType(
                new ParameterType<>("propertyOrValue", ".*?", StringPropOrValue.class, StringPropOrValue::new)
        );
        typeRegistry.defineParameterType(new ParameterType<>("key", ".*?", Keys.class, (Transformer<Keys>) Keys::valueOf));
    }
}
