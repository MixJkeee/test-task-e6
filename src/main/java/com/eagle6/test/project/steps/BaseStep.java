package com.eagle6.test.project.steps;


import com.eagle6.test.project.pages.BasePage;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.function.Consumer;

import static com.eagle6.test.project.steps.Hooks.getDriver;
import static com.eagle6.test.project.util.PropertyReader.getApplicationProperties;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;
import static ru.yandex.qatools.htmlelements.loader.HtmlElementLoader.createHtmlElement;
import static ru.yandex.qatools.htmlelements.loader.HtmlElementLoader.createPageObject;

@Log4j2
public class BaseStep {
    protected final Map<String, Object> scenarioVariables = new HashMap<>();
    protected WebDriver driver = getDriver();
    protected Properties applicationProperties = getApplicationProperties();

    protected String getPropertyOrStringVariableOrValue(String propOrVarOrValue) {
        String fromProperty = applicationProperties.getProperty(requireNonNull(propOrVarOrValue));
        if (null != fromProperty) {
            log.debug("Found property {}", fromProperty);
            return fromProperty;
        }
        log.debug("Property with key {} is not found, trying to get from variables", propOrVarOrValue);
        return ofNullable(scenarioVariables.get(propOrVarOrValue))
                .map(Objects::toString)
                .orElse(propOrVarOrValue);
    }

    protected <T extends BasePage> void withPage(Class<T> clazz, Consumer<T> action) {
        T page = createPageObject(clazz, driver);
        action.accept(page);
    }

    protected <T extends HtmlElement> void withElement(Class<T> clazz, SearchContext searchContext, Consumer<T> action) {
        T element = createHtmlElement(clazz, searchContext);
        action.accept(element);
    }

    protected <T extends HtmlElement> void withElement(Class<T> clazz, Consumer<T> action) {
        withElement(clazz, driver, action);
    }

    protected Actions actions() {
        return new Actions(driver);
    }
}
