package com.eagle6.test.project.model;

import lombok.Data;

import static com.eagle6.test.project.util.PropertyReader.getPropertyOrValue;

@Data
public class StringPropOrValue {
    private String value;

    public StringPropOrValue(String propertyOrValue) {
        value = getPropertyOrValue(propertyOrValue);
    }
}
