package com.eagle6.test.project.model;

import com.eagle6.test.project.util.CollectionsUtils;
import com.eagle6.test.project.util.PropertyReader;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

import static com.eagle6.test.project.util.CollectionsUtils.getFromMapIgnoringCase;
import static com.eagle6.test.project.util.PropertyReader.getPropertyOrValue;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String userName;
    private String email;
    private String password;

    public static User createUser(Map<String, String> tableEntry) {
        return User.builder()
                .email(getPropertyOrValue(getFromMapIgnoringCase(tableEntry, "email")))
                .userName(getPropertyOrValue(getFromMapIgnoringCase(tableEntry, "username")))
                .password(getPropertyOrValue(getFromMapIgnoringCase(tableEntry, "password")))
                .build();
    }
}
