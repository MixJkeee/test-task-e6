package com.eagle6.test.project.components.board;

import com.eagle6.test.project.components.BaseComponent;
import lombok.Getter;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

@Getter
@Name("Create card component")
@FindBy(css = ".card-composer")
public class CardComposer extends BaseComponent {

    @Name("Card title")
    @FindBy(css = "textarea")
    private TextInput cardTitleInput;

    @Name("Create card button")
    @FindBy(css = "input[type = 'submit']")
    private Button addCardButton;

    @Name("Close composer button")
    @FindBy(css = ".icon-close")
    private Button closeComposer;

    public CardComposer typeCardName(String cardName) {
        return setInputText(cardTitleInput, cardName);
    }

    public CardComposer addCard(String cardName) {
        return typeCardName(cardName).click(addCardButton);
    }
}
