package com.eagle6.test.project.components;

import com.eagle6.test.project.util.WebElementsUtils;
import org.hamcrest.Matcher;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextInput;
import ru.yandex.qatools.htmlelements.matchers.WebElementMatchers;

import static com.eagle6.test.project.util.WebElementsUtils.waitFor;

@SuppressWarnings("unchecked")
public class BaseComponent extends HtmlElement {
    //added these constants because static import is not possible
    protected static final Matcher<WebElement> visible = WebElementMatchers.isDisplayed();
    protected static final Matcher<WebElement> enabled = WebElementMatchers.isEnabled();
    protected static final Matcher<WebElement> exist = WebElementMatchers.exists();


    public <T extends BaseComponent> T click(WebElement element) {
        WebElementsUtils.click(element);
        return (T) this;
    }

    public <T extends BaseComponent> T typeText(TextInput element, String text) {
        WebElementsUtils.sendKeys(element, text);
        return (T) this;
    }

    protected <T extends BaseComponent> T clearInput(TextInput input) {
        WebElementsUtils.clear(input);
        return (T) this;
    }

    protected <T extends BaseComponent> T setInputText(TextInput input, String text) {
        return clearInput(input).typeText(input, text);
    }

    @Override
    public void click() {
        waitFor(this, enabled);
        super.click();
    }

    @Override
    public boolean isDisplayed() {
        return exists() && super.isDisplayed();
    }


}
