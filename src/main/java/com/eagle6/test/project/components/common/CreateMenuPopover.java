package com.eagle6.test.project.components.common;

import com.eagle6.test.project.components.BaseComponent;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;

@Getter
@Log4j2
@FindBy(xpath = "descendant::*[@data-test-id = 'header-create-menu-popover']")
public class CreateMenuPopover extends BaseComponent {

    @Name("Close button")
    @FindBy(xpath = "descendant::*[@data-test-id = 'popover-close']")
    private Button closeButton;

    @Name("Create board button")
    @FindBy(xpath = "descendant::*[@data-test-id = 'header-create-board-button']")
    private Button createBoardButton;

    @Name("Create team button")
    @FindBy(xpath = "descendant::*[@data-test-id = 'header-create-team-button']")
    private Button createTeamButton;

    @Name("Create business team button")
    @FindBy(xpath = "descendant::*[@data-test-id = 'header-create-business-team-button']")
    private Button createBusinessTeamButton;

}
