package com.eagle6.test.project.components.board;

import com.eagle6.test.project.components.BaseComponent;
import lombok.Getter;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;

@Getter
@FindBy(css = ".pop-over")
public class ListMenu extends BaseComponent {

    @Name("Add card button")
    @FindBy(css = ".js-add-card")
    private Button addCardButton;

    @Name("Copy list button")
    @FindBy(css = ".js-copy-list")
    private Button copyListButton;

    @Name("Move list button")
    @FindBy(css = ".js-move-list")
    private Button moveListButton;

    @Name("Subscribe on list button")
    @FindBy(css = ".js-list-subscribe")
    private Button subscribeOnListButton;

    @Name("Move cards button")
    @FindBy(css = ".js-move-cards")
    private Button moveCardsButton;

    @Name("Archive cards button")
    @FindBy(css = ".js-archive-cards")
    private Button archiveCardsButton;

    @Name("Archive list button")
    @FindBy(css = ".js-close-list")
    private Button archiveListButton;
}
