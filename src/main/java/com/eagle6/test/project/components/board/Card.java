package com.eagle6.test.project.components.board;

import com.eagle6.test.project.components.BaseComponent;
import lombok.Getter;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.TextBlock;

@Getter
@Name("Card")
@FindBy(css = "a.list-card")
public class Card extends BaseComponent {

    //todo: add a separate class Icon extends TypifiedElement
    @Name("Icon edit")
    @FindBy(css = ".icon-edit")
    private TextBlock editIcon;

    @Name("Title")
    @FindBy(css = ".list-card-title")
    private TextBlock cardTitle;
}
