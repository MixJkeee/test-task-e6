package com.eagle6.test.project.components.board;

import com.eagle6.test.project.components.BaseComponent;
import lombok.Getter;
import org.openqa.selenium.support.FindBy;

//todo: fill component
@Getter
@FindBy(css = ".board-header")
public class BoardHeader extends BaseComponent {
}
