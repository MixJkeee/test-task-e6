package com.eagle6.test.project.components.login;

import com.eagle6.test.project.components.BaseComponent;
import lombok.Getter;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

import static com.eagle6.test.project.util.WebElementsUtils.waitFor;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static ru.yandex.qatools.htmlelements.matchers.WebElementMatchers.hasValue;

@Getter
@Name("Trello login form")
@FindBy(id = "login-form")
public class TrelloLoginForm extends BaseComponent {

    @Name("Username input")
    @FindBy(id = "user")
    private TextInput user;

    @Name("Password input")
    @FindBy(id = "password")
    private TextInput password;

    @Name("Submit button")
    @FindBy(id = "login")
    private Button login;

    public void openAtlassianLoginPageAs(String email) {
        typeText(user, requireNonNull(email));
        waitFor(password, not(visible));
        waitFor(login, hasValue(containsString("Atlassian")));
        click(login);
    }
}
