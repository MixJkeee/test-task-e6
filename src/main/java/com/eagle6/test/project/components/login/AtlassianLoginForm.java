package com.eagle6.test.project.components.login;

import com.eagle6.test.project.components.BaseComponent;
import com.eagle6.test.project.model.User;
import lombok.Getter;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

import static com.eagle6.test.project.util.WebElementsUtils.waitFor;
import static java.util.Objects.requireNonNull;

@Getter
@Name("Atlassian login form")
@FindBy(id = "form-login")
public class AtlassianLoginForm extends BaseComponent {

    @Name("Username input")
    @FindBy(id = "username")
    private TextInput userName;

    @Name("Password input")
    @FindBy(id = "password")
    private TextInput password;

    @Name("Submit button")
    @FindBy(id = "login-submit")
    private Button submit;

    public void loginAs(User user) {
        if (!waitFor(userName, visible).getText().equals(requireNonNull(user.getEmail()))) {
            setInputText(userName, user.getEmail());
        }
        click(submit);
        setInputText(password, requireNonNull(user.getPassword()));
        click(submit);
    }
}
