package com.eagle6.test.project.components.common;

import com.eagle6.test.project.components.BaseComponent;
import lombok.Getter;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextInput;

@Getter
@Name("Header")
@FindBy(xpath = "descendant::*[@data-test-id = 'header-container']")
public class Header extends BaseComponent {

    @Name("Home button")
    @FindBy(xpath = "descendant::*[child::*[@name = 'house']]")
    private Link home;

    @Name("Boards page button")
    @FindBy(xpath = "descendant::*[@data-test-id = 'header-boards-menu-button']")
    private Button toBoardsPage;

    @Name("Search input")
    @FindBy(xpath = "descendant::*[@data-test-id = 'header-search-input']")
    private TextInput searchInput;

    @Name("Create button")
    @FindBy(xpath = "descendant::*[@data-test-id = 'header-create-menu-button']")
    private Button createButton;

    @Name("Info button")
    @FindBy(xpath = "descendant::*[@data-test-id = 'header-info-button']")
    private Button infoButton;

    @Name("Notifications button")
    @FindBy(xpath = "descendant::*[@data-test-id = 'header-notifications-button']")
    private Button notificationsButton;
}
