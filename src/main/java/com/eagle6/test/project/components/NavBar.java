package com.eagle6.test.project.components;

import lombok.Getter;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.Link;

@Getter
@FindBy(css = "nav.navbar")
public class NavBar extends BaseComponent {

    @FindBy(css = "a[href='/login']")
    private Link login;

    @FindBy(css = "a[href='/signup']")
    private Link signUp;
}
