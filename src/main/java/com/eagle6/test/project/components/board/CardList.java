package com.eagle6.test.project.components.board;

import com.eagle6.test.project.components.BaseComponent;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;

import java.util.List;

import static com.eagle6.test.project.util.WebElementsUtils.waitFor;
import static java.util.stream.Collectors.toList;

@Getter
@Log4j2
@FindBy(css = ".js-list")
public class CardList extends BaseComponent {

    @Name("List name text")
    @FindBy(css = ".list-header-name-assist")
    private TextBlock listName;

    @Name("List name input")
    @FindBy(css = "textarea.list-header-name")
    private TextInput listNameInput;

    @Name("Open list menu")
    @FindBy(css = ".list-header-extras-menu")
    private Button openMenuButton;

    @Name("Cards")
    private List<Card> cards;

    @Name("Card composer")
    private CardComposer cardComposer;

    @Name("Open composer button")
    @FindBy(css = ".open-card-composer")
    private Link openComposer;

    public CardList createNewCard(String cardName) {
        openCardComposer().addCard(cardName);
        return this;
    }

    public CardComposer openCardComposer() {
        waitFor(this, visible);
        if (openComposer.isDisplayed()) {
            click(openComposer);
        }
        return cardComposer;
    }

    public List<String> getDisplayedCardsNames() {
        return waitFor(this, visible).cards.stream()
                .filter(HtmlElement::isDisplayed)
                .map(Card::getCardTitle)
                .map(TypifiedElement::getText)
                .collect(toList());
    }

    public String getListNameText() {
        return waitFor(listName, exist).getAttribute("textContent");
    }

}
