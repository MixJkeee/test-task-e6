package com.eagle6.test.project.components.common;

import com.eagle6.test.project.components.BaseComponent;
import lombok.Getter;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

@Getter
@FindBy(xpath = "descendant::*[parent::*[@role = 'dialog'] and descendant::*[@data-test-id = 'create-board-title-input']]")
public class CreateBoardPopover extends BaseComponent {

    @FindBy(name = "close")
    private Button closePopover;

    @FindBy(xpath = "descendant::*[@data-test-id = 'create-board-title-input']")
    private TextInput boardTitleInput;

    @FindBy(xpath = "descendant::*[@data-test-id = 'create-board-submit-button']")
    private Button createBoardButton;

    public void createBoardWithName(String boardName) {
        typeText(boardTitleInput, boardName).click(createBoardButton);
    }
}
