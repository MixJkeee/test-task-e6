package com.eagle6.test.project.components.board;

import com.eagle6.test.project.components.BaseComponent;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

import java.util.List;

import static com.eagle6.test.project.util.WebElementsUtils.*;
import static java.util.stream.Collectors.toList;

@Getter
@Log4j2
@FindBy(id = "board")
public class BoardContent extends BaseComponent {

    @Name("Button add list")
    @FindBy(css = ".js-add-list")
    private Button addListButton;

    @Name("List name input")
    @FindBy(css = "input.list-name-input")
    private TextInput listNameInput;

    @Name("Create list button")
    @FindBy(css = ".list-add-controls input[type='submit']")
    private Button addButton;

    private List<CardList> cardLists;
    private ListMenu listMenu;

    public BoardContent createNewList(String listName) {
        return openCreateListMenu()
                .setListName(listName)
                .click(addButton);
    }

    public BoardContent createCardInList(String listName, String cardName) {
        findCardList(listName).createNewCard(cardName);
        return this;
    }

    public BoardContent openCardListMenu(String listName) {
        CardList list = findCardList(listName);
        click(list.getOpenMenuButton());
        return this;
    }

    public CardList findCardList(String listName) {
        return findElementInList(cardLists, cardList -> cardList.getListNameText().equals(listName));
    }

    public BoardContent checkCardListIsNotDisplayed(String listName) {
        waitForAllElementsMatchingCondition(cardLists, cardList -> !cardList.getListNameText().equals(listName));
        return this;
    }

    public BoardContent setListName(String listName) {
        return setInputText(listNameInput, listName);
    }

    public BoardContent openCreateListMenu() {
        if (!waitFor(this, visible).listNameInput.isDisplayed()) {
            click(addListButton);
        }
        return this;
    }

    public List<String> getCardsListsNames() {
        List<String> cardListNames = waitFor(this, visible).cardLists.stream()
                .map(CardList::getListNameText)
                .collect(toList());
        log.info("Card lists names {}", cardListNames);
        return cardListNames;
    }

}
