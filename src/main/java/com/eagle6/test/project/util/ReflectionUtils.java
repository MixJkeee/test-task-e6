package com.eagle6.test.project.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.lang.reflect.Field;

import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;

public class ReflectionUtils {
    private ReflectionUtils() {
    }

    @SuppressWarnings("unchecked")
    public static WebElement getWebElementByName(Object searchContext, String name) {
        return stream(requireNonNull(searchContext).getClass().getDeclaredFields())
                .filter(field -> WebElement.class.isAssignableFrom(field.getType()))
                .filter(field -> field.isAnnotationPresent(Name.class))
                .filter(field -> field.getAnnotation(Name.class).value().equals(name))
                .findFirst()
                .map(field -> getFieldValue(searchContext, field))
                .map(WebElement.class::cast)
                .orElseThrow(() -> new IllegalArgumentException("Unable to find field with name " + name));
    }

    @SneakyThrows(IllegalAccessException.class)
    private static Object getFieldValue(Object obj, @NotNull Field field) {
        field.setAccessible(true);
        return field.get(obj);
    }
}
