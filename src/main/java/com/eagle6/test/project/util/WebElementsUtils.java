package com.eagle6.test.project.util;

import org.hamcrest.Matcher;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import ru.yandex.qatools.htmlelements.element.CheckBox;

import java.time.Duration;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static com.eagle6.test.project.util.PropertyReader.getLongApplicationProperty;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.Description.NONE;
import static org.hamcrest.Matchers.allOf;
import static ru.yandex.qatools.htmlelements.matchers.WebElementMatchers.isDisplayed;
import static ru.yandex.qatools.htmlelements.matchers.WebElementMatchers.isEnabled;

public class WebElementsUtils {
    private static final long DEFAULT_WAIT_TIME_MILLIS = 5000;
    private static final long DEFAULT_POLLING_INTERVAL = 500;
    private static final String POLLING_INTERVAL_PROP = "pollingInterval";
    private static final String WAIT_TIME_MILLIS_PROP = "waitTimeMillis";

    private static Supplier<Long> getWaitTimeMillisFunc = () -> getLongApplicationProperty(WAIT_TIME_MILLIS_PROP, DEFAULT_WAIT_TIME_MILLIS);
    private static Supplier<Long> getPollingIntervalFunc = () -> getLongApplicationProperty(POLLING_INTERVAL_PROP, DEFAULT_POLLING_INTERVAL);

    private WebElementsUtils() {
    }

    public static <T> FluentWait<T> getWait(T item) {
        return getWait(item, getWaitTimeMillisFunc.get());
    }

    public static <T> FluentWait<T> getWait(T item, long timeoutMillis) {
        return new FluentWait<>(item)
                .withTimeout(Duration.ofMillis(timeoutMillis))
                .pollingEvery(Duration.ofMillis(getPollingIntervalFunc.get()));
    }

    public static <T extends WebElement> T waitFor(T element, Matcher<? super T> condition, long waitTimeMillis) {
        Function<T, T> isTrueFunc = new Function<T, T>() {
            @Override
            public T apply(T t) {
                return requireNonNull(condition).matches(t) ? t : null;
            }

            @Override
            public String toString() {
                condition.describeTo(NONE);
                return condition.toString();
            }
        };
        return getWait(element, waitTimeMillis).ignoring(WebDriverException.class)
                .until(isTrueFunc);
    }

    public static <T extends WebElement> T waitFor(T element, Matcher<? super T> condition) {
        return waitFor(element, condition, getWaitTimeMillisFunc.get());
    }

    public static <T extends WebElement> List<T> waitFor(List<T> elements, Matcher<? super List<T>> condition, long waitTimeMillis) {
        return getWait(elements, waitTimeMillis).ignoring(WebDriverException.class).until(elems -> condition.matches(elems) ? elems : null);
    }

    public static <T extends WebElement> List<T> waitFor(List<T> elements, Matcher<? super List<T>> condition) {
        return waitFor(elements, condition, getWaitTimeMillisFunc.get());
    }

    public static void click(WebElement element) {
        waitFor(element, allOf(isDisplayed(), isEnabled())).click();
    }

    public static <T extends WebElement> void sendKeys(T element, CharSequence... keysToSend) {
        waitFor(element, allOf(isDisplayed(), isEnabled())).sendKeys(keysToSend);
    }

    public static void clear(WebElement element) {
        waitFor(element, allOf(isDisplayed(), isEnabled())).clear();
    }

    public static void setChecked(CheckBox checkBox, boolean value) {
        waitFor(checkBox, allOf(isDisplayed(), isEnabled())).set(value);
    }

    public static <T extends WebElement> T findElementInList(List<T> list, Predicate<T> condition) {
        return getWait(list).ignoring(WebDriverException.class)
                .until(ls -> ls.stream().filter(condition).findFirst().orElse(null));
    }

    public static <T extends WebElement> List<T> waitForAllElementsMatchingCondition(List<T> list, Predicate<T> condition) {
        return getWait(list).ignoring(WebDriverException.class).until(ls -> list.stream().allMatch(condition) ? ls : null);
    }

}
