package com.eagle6.test.project.util;

import java.util.Map;

import static java.util.Objects.requireNonNull;

public class CollectionsUtils {
    private CollectionsUtils() {
    }

    public static <T> T getFromMapIgnoringCase(Map<String, T> map, String key, T defaultValue) {
        requireNonNull(key);
        return map.entrySet().stream()
                .filter(entry -> entry.getKey().equalsIgnoreCase(key))
                .findFirst().map(Map.Entry::getValue).orElse(defaultValue);
    }

    public static <T> T getFromMapIgnoringCase(Map<String, T> map, String key) {
        return getFromMapIgnoringCase(map, key, null);
    }
}
