package com.eagle6.test.project.util;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static com.eagle6.test.project.util.PropertyReader.*;
import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static io.github.bonigarcia.wdm.WebDriverManager.firefoxdriver;
import static io.vavr.API.*;
import static java.lang.Runtime.getRuntime;
import static java.lang.System.getProperty;
import static java.lang.System.setProperty;
import static java.util.Optional.ofNullable;
import static java.util.concurrent.TimeUnit.SECONDS;

public class WebDriverFactory {

    private static final String CHROME = "chrome";
    private static final String FIREFOX = "firefox";

    static {
        setProperty("webdriver.timeouts.implicitlywait", getApplicationProperty("implicitWait", "0"));
    }

    private static long implicitWait = getLongApplicationProperty("implicitWait", 0);
    private static boolean startMaximized = getBooleanApplicationProperty("startMaximized", false);
    private static long pageLoadTimeout = getLongApplicationProperty("pageLoadTimeout", 10);
    private static long scriptTimeout = getLongApplicationProperty("scriptTimeout", 2);

    public static WebDriver getWebDriver() {
        String browser = ofNullable(getProperty("browser")).orElse(getApplicationProperty("browser"));
        return Match(browser).of(
                Case($(StringUtils::isBlank), () -> {
                    throw new IllegalArgumentException("Browser name should be specified in program arguments or in application.properties file");
                }),
                Case($(CHROME::equalsIgnoreCase), () -> configureDriver(createChromeDriver())),
                Case($(FIREFOX::equalsIgnoreCase), () -> configureDriver(createFireFoxDriver())),
                Case($(), () -> {
                    throw new IllegalArgumentException("Browser is not recognized: " + browser);
                })
        );
    }

    private static WebDriver configureDriver(WebDriver driver) {
        getRuntime().addShutdownHook(new Thread(driver::quit));
        driver.manage().timeouts().implicitlyWait(implicitWait, SECONDS);
        driver.manage().timeouts().pageLoadTimeout(pageLoadTimeout, SECONDS);
        driver.manage().timeouts().setScriptTimeout(scriptTimeout, SECONDS);
        if (startMaximized) {
            driver.manage().window().maximize();
        }
        return driver;
    }

    private static ChromeDriver createChromeDriver() {
        chromedriver().setup();
        return new ChromeDriver();
    }

    private static FirefoxDriver createFireFoxDriver() {
        firefoxdriver().setup();
        return new FirefoxDriver();
    }
}
