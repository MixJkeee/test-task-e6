package com.eagle6.test.project.util;

import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.Boolean.parseBoolean;
import static java.lang.Long.parseLong;
import static java.lang.Thread.currentThread;

@Log4j2
public class PropertyReader {
    private PropertyReader() {
    }

    public static Properties getProperties(String propertyFileName) {
        try (InputStream is = currentThread().getContextClassLoader().getResourceAsStream(propertyFileName)) {
            Properties properties = new Properties();
            properties.load(is);
            return properties;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Properties getApplicationProperties() {
        return getProperties("application.properties");
    }

    public static String getApplicationProperty(String propertyName, String defaultVal) {
        if (null == propertyName)
            return defaultVal;
        return getApplicationProperties().getProperty(propertyName, defaultVal);
    }

    public static String getApplicationProperty(String propertyName) {
        return getApplicationProperty(propertyName, null);
    }

    public static Long getLongApplicationProperty(String propertyName, long defaultVal) {
        return parseLong(getApplicationProperty(propertyName, String.valueOf(defaultVal)));
    }

    public static Boolean getBooleanApplicationProperty(String propertyName, boolean defaultval) {
        return parseBoolean(getApplicationProperty(propertyName, String.valueOf(defaultval)));
    }

    public static String getPropertyOrValue(String propertyOrValue) {
        String result = getApplicationProperty(propertyOrValue, propertyOrValue);
        log.debug("Result property or value: {}", result);
        return result;
    }
}
