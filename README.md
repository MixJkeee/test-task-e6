# TEST TASK E6

## Web ui automation project for testing Trello written on Java + Selenium + Cucumber 4 + Gradle + Yandex HtmlElements + Allure

To run all tests with generating report use gradle command:
`./gradlew clean test allureReport --continue`

"--continue" flag is used for generating report regardless of "test" task result (`test.finalizedBy(allureReport)` doesn't work)

To run tests with specified tag use `-Ptag=` gradle property. Example:

`./gradlew clean test allureReport --continue -Ptag=@createLists`

Available tags are: 

* "@all"
* "@createLists" 
* "@archiveLists"
* "@createCards"

To see the generated report use `./gradlew allureServe` command or open generated report in your IDE in project build directory (/build/reports/allure-report/)

To run tests in another browser use `-Pbrowser=` gradle property (only chrome and firefox are supported now)